json.extract! workplace, :id, :name, :address, :request_limit, :created_by, :created_at, :updated_at
json.url workplace_url(workplace, format: :json)
