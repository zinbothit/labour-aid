json.extract! application_request, :id, :work_place_id, :status, :verified_at, :rejected_at, :approved_at, :verified_by, :approved_by, :employee_list, :created_at, :updated_at
json.url application_request_url(application_request, format: :json)
