require 'rails_helper'

RSpec.describe "workplaces/index", type: :view do
  before(:each) do
    assign(:workplaces, [
      Workplace.create!(
        name: "Name",
        address: "MyText",
        request_limit: 2,
        created_by: 3
      ),
      Workplace.create!(
        name: "Name",
        address: "MyText",
        request_limit: 2,
        created_by: 3
      )
    ])
  end

  it "renders a list of workplaces" do
    render
    assert_select "tr>td", text: "Name".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: 3.to_s, count: 2
  end
end
