require 'rails_helper'

RSpec.describe "workplaces/edit", type: :view do
  before(:each) do
    @workplace = assign(:workplace, Workplace.create!(
      name: "MyString",
      address: "MyText",
      request_limit: 1,
      created_by: 1
    ))
  end

  it "renders the edit workplace form" do
    render

    assert_select "form[action=?][method=?]", workplace_path(@workplace), "post" do

      assert_select "input[name=?]", "workplace[name]"

      assert_select "textarea[name=?]", "workplace[address]"

      assert_select "input[name=?]", "workplace[request_limit]"

      assert_select "input[name=?]", "workplace[created_by]"
    end
  end
end
