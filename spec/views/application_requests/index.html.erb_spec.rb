require 'rails_helper'

RSpec.describe "application_requests/index", type: :view do
  before(:each) do
    assign(:application_requests, [
      ApplicationRequest.create!(
        work_place_id: 2,
        status: "Status",
        verified_by: 3,
        approved_by: 4,
        employee_list: ""
      ),
      ApplicationRequest.create!(
        work_place_id: 2,
        status: "Status",
        verified_by: 3,
        approved_by: 4,
        employee_list: ""
      )
    ])
  end

  it "renders a list of application_requests" do
    render
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: "Status".to_s, count: 2
    assert_select "tr>td", text: 3.to_s, count: 2
    assert_select "tr>td", text: 4.to_s, count: 2
    assert_select "tr>td", text: "".to_s, count: 2
  end
end
