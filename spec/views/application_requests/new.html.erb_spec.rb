require 'rails_helper'

RSpec.describe "application_requests/new", type: :view do
  before(:each) do
    assign(:application_request, ApplicationRequest.new(
      work_place_id: 1,
      status: "MyString",
      verified_by: 1,
      approved_by: 1,
      employee_list: ""
    ))
  end

  it "renders new application_request form" do
    render

    assert_select "form[action=?][method=?]", application_requests_path, "post" do

      assert_select "input[name=?]", "application_request[work_place_id]"

      assert_select "input[name=?]", "application_request[status]"

      assert_select "input[name=?]", "application_request[verified_by]"

      assert_select "input[name=?]", "application_request[approved_by]"

      assert_select "input[name=?]", "application_request[employee_list]"
    end
  end
end
