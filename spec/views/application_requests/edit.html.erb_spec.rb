require 'rails_helper'

RSpec.describe "application_requests/edit", type: :view do
  before(:each) do
    @application_request = assign(:application_request, ApplicationRequest.create!(
      work_place_id: 1,
      status: "MyString",
      verified_by: 1,
      approved_by: 1,
      employee_list: ""
    ))
  end

  it "renders the edit application_request form" do
    render

    assert_select "form[action=?][method=?]", application_request_path(@application_request), "post" do

      assert_select "input[name=?]", "application_request[work_place_id]"

      assert_select "input[name=?]", "application_request[status]"

      assert_select "input[name=?]", "application_request[verified_by]"

      assert_select "input[name=?]", "application_request[approved_by]"

      assert_select "input[name=?]", "application_request[employee_list]"
    end
  end
end
