require 'rails_helper'

RSpec.describe "application_requests/show", type: :view do
  before(:each) do
    @application_request = assign(:application_request, ApplicationRequest.create!(
      work_place_id: 2,
      status: "Status",
      verified_by: 3,
      approved_by: 4,
      employee_list: ""
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Status/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(//)
  end
end
