Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }
  resources :application_requests
  resources :workplaces
  get 'home/index'
  get 'users', to: "users#index"
	get 'users/new' => "users#new", as: :new_user
	post 'create_user' => "users#create_user", as: :create_user
  root 'workplaces#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
